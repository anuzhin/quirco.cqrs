﻿using System.Security.Claims;
using System.Security.Principal;

namespace Quirco.Cqrs.Domain.Security;

/// <summary>
/// Extensions for the <see cref="IPrincipal"/>.
/// </summary>
public static class PrincipalExtensions
{
    /// <summary>
    /// Returns user's email
    /// </summary>
    /// <param name="principal"></param>
    /// <returns></returns>
    public static string? GetEmail(this IPrincipal principal)
    {
        var claimsPrincipal = principal as ClaimsPrincipal;
        return claimsPrincipal?.FindFirstValue("email") ?? claimsPrincipal?.FindFirstValue(ClaimTypes.Email);
    }

    /// <summary>
    /// Returns user's phone
    /// </summary>
    /// <param name="principal"></param>
    /// <returns></returns>
    public static string? GetPhone(this IPrincipal principal)
    {
        var claimsPrincipal = principal as ClaimsPrincipal;
        return claimsPrincipal?.FindFirstValue("phone_number") ??
               claimsPrincipal?.FindFirstValue(ClaimTypes.MobilePhone);
    }

    /// <summary>
    /// Checks if user has id
    /// </summary>
    /// <param name="principal"></param>
    /// <returns></returns>
    public static bool HasSubjectId(this IPrincipal principal)
    {
        var id = principal.Identity as ClaimsIdentity;
        var claim = id?.FindFirst("sub");
        return claim != null && !string.IsNullOrEmpty(claim.Value);
    }

    /// <summary>
    /// Check if user has roles
    /// </summary>
    /// <param name="principal"></param>
    /// <param name="roles"></param>
    /// <returns></returns>
    public static bool IsInRole(this IPrincipal principal, params string[] roles)
    {
        if (principal == null)
        {
            throw new ArgumentNullException(nameof(principal));
        }

        if (roles == null)
        {
            throw new ArgumentNullException(nameof(roles));
        }

        return roles.Aggregate(false, (result, role) => result || principal.IsInRole(role));
    }
}