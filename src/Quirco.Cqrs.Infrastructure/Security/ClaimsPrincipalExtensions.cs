using System.Security.Claims;

namespace Quirco.Cqrs.Domain.Security;

public static class ClaimsPrincipalExtensions
{
    public static string? FindFirstValue(this ClaimsPrincipal principal, string claimName)
    {
        return principal.FindFirst(claimName)?.Value;
    }
}