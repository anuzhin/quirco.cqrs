﻿using Microsoft.Extensions.DependencyInjection;

namespace Quirco.Cqrs.Infrastructure.Mediatr;

/// <summary>
/// Фабрика для создания экземпляров IMediator. Используется для ручного управления временем жизни медиатора и всех зависимостей команд и запросов.
/// </summary>
public class MediatorFactory
{
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public MediatorFactory(IServiceScopeFactory serviceScopeFactory)
    {
        _serviceScopeFactory = serviceScopeFactory;
    }

    /// <summary>
    /// Создаёт новый скоуп для медиатора. Следует освобождать его после использования.
    /// </summary>
    /// <returns></returns>
    public MediatorScope CreateScope() => new(_serviceScopeFactory.CreateScope());
}