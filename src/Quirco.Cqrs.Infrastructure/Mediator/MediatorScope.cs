using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Quirco.Cqrs.Infrastructure.Mediatr;

/// <summary>
/// Скоуп жизни медиатора. После использования следует освободить.
/// </summary>
public class MediatorScope : IDisposable
{
    private readonly IServiceScope _serviceScope;

    private IMediator? _mediator;

    public IMediator Mediator => _mediator ??= _serviceScope.ServiceProvider.GetRequiredService<IMediator>();

    public MediatorScope(IServiceScope serviceScope)
    {
        _serviceScope = serviceScope;
    }
        
    public void Dispose()
    {
        _serviceScope.Dispose();
    }
}