using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;

namespace Quirco.Cqrs.Infrastructure.Jobs;

/// <summary>
/// Background jobs service
/// </summary>
public interface IJobsService
{
    /// <summary>
    /// Equeue job to run
    /// </summary>
    /// <param name="methodCall"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    Task<string> Enqueue<T>([NotNull] Expression<Func<T, Task>> methodCall);

    /// <summary>
    /// Schedule job to execute on cron
    /// </summary>
    /// <param name="id">Job unique id</param>
    /// <param name="methodCall">Job method</param>
    /// <param name="cronExpression">Cron expression</param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    Task Schedule<T>(string id, [NotNull] Expression<Func<T, Task>> methodCall, string cronExpression);
        
    /// <summary>
    /// Schedule job to execute after some delay.
    /// </summary>
    /// <param name="methodCall">Job method</param>
    /// <param name="delay">TimeSpan to delay job execution</param>
    Task<string> Schedule<T>([NotNull] Expression<Func<T, Task>> methodCall, TimeSpan delay);
}