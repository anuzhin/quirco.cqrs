namespace Quirco.Cqrs.Elasticsearch.Behaviors;

public class SearchPipelineContext
{
    public bool SkipElasticsearch { get; set; }
}