using MediatR;
using Nest;
using Quirco.Cqrs.Domain.Models;

namespace Quirco.Cqrs.Elasticsearch.Behaviors;

/// <summary>
/// Base pipeline to use Elastic as search engine
/// </summary>
/// <typeparam name="TQuery"></typeparam>
/// <typeparam name="TModel"></typeparam>
public abstract class SearchPipelineBehavior<TQuery, TModel> : IPipelineBehavior<TQuery, PagedResponse<TModel>>
    where TModel: class where TQuery : MediatR.IRequest<PagedResponse<TModel>>
{
    protected readonly SearchPipelineContext Context;

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchPipelineBehavior{TQuery, TModel}"/>.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="modifiers"></param>
    protected SearchPipelineBehavior(SearchPipelineContext context, IEnumerable<IQueryContainerModifier<TModel>> modifiers)
    {
        Context = context;
        Modifiers = modifiers;
    }

    private IEnumerable<IQueryContainerModifier<TModel>> Modifiers { get; }
        
    /// <inheritdoc />
    public abstract Task<PagedResponse<TModel>> Handle(
        TQuery request,
        CancellationToken cancellationToken,
        RequestHandlerDelegate<PagedResponse<TModel>> next);

    /// <summary>
    /// Накладывает необходимые фильтры.
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    protected QueryContainer Modify(QueryContainerDescriptor<TModel> query)
    {
        return Modifiers.Aggregate((QueryContainer) query, (acc, modifier) => acc & modifier.Modify(query));
    }
}