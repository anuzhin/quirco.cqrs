using MediatR;

namespace Quirco.Cqrs.Elasticsearch.Commands.IndexAll;

/// <summary>
/// Base command for reindexing database table.
/// </summary>
public abstract class IndexAllCommand : IRequest
{
    /// <summary>
    /// Batch size.
    /// </summary>
    public int BatchSize { get; set; } = 512;
}