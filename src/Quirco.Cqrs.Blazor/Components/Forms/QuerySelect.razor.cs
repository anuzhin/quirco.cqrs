using MediatR;
using Microsoft.AspNetCore.Components;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.Queries;

namespace Quirco.Cqrs.Blazor.Components.Forms;

public partial class QuerySelect<TItem, TItemValue, TQuery> where TQuery : SearchQuery<TItem>, new()
{
    /// <summary>
    /// Template for select item
    /// </summary>
    [Parameter]
    public RenderFragment<TItem> ItemTemplate { get; set; }

    /// <summary>
    /// Limit search results
    /// </summary>
    [Parameter]
    public int Limit { get; set; } = 20;

    /// <summary>
    /// Initial value for select control
    /// </summary>
    [Parameter]
    public TItem? InitialValue { get; set; }

    /// <summary>
    /// The name of the property to be used for the value.
    /// </summary>
    [Parameter]
    public string ValueName { get; set; } = "Id";

    /// <summary>
    /// The name of the property to be used for the label.
    /// </summary>
    [Parameter]
    public string LabelName { get; set; } = "Name";

    /// <summary>
    /// Placeholder text
    /// </summary>
    [Parameter]
    public string Placeholder { get; set; }

    /// <summary>
    /// Имя компонента в DOM-дереве
    /// </summary>
    [Parameter]
    public string DomName { get; set; }


    [Parameter] public bool EnableSearch { get; set; } = true;

    /// <summary>
    /// Loading indicator
    /// </summary>
    protected bool IsLoading { get; set; }

    [Parameter] public bool Disabled { get; set; }

    private TItemValue _selectedValue;

    /// <summary>
    /// Get or set the selected value.
    /// </summary>
    [Parameter]
    public override TItemValue Value
    {
        get => _selectedValue;
        set
        {
            var valueHasChanged = !EqualityComparer<TItemValue>.Default.Equals(value, _selectedValue);
            if (valueHasChanged)
            {
                _selectedValue = value;
                ValueChanged.InvokeAsync(value);
            }
        }
    }

    /// <summary>
    /// Modifier for search request
    /// </summary>
    [Parameter]
    public Action<TQuery>? QueryModifier { get; set; }

    /// <summary>
    /// Called when the selected item changes.
    /// </summary>
    [Parameter]
    public Action<TItem> OnSelectedItemChanged { get; set; }

    [Inject] public IMediator Mediator { get; set; }

    protected TItem[] Items { get; set; } = Array.Empty<TItem>();

    private bool _preloaded = false;

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();
        if (InitialValue != null && !Items.Any(i => ItemsAreEqual(i, InitialValue)))
        {
            Items = Items.Concat(new[] { InitialValue }).ToArray();
        }
    }

    /// <summary>
    /// Compares to models with ID support
    /// </summary>
    /// <param name="item1"></param>
    /// <param name="item2"></param>
    /// <returns></returns>
    private static bool ItemsAreEqual(TItem item1, TItem item2)
    {
        if (item1 is IHasId item1Id && item2 is IHasId item2Id)
        {
            return item1Id.Id.Equals(item2Id.Id);
        }

        return Equals(item1, item2);
    }

    private void OnSearch(string query)
    {
        if (!EnableSearch)
            return;
        ReloadItems(query);
    }

    /// <summary>
    /// Loads top items regarding query
    /// </summary>
    /// <param name="query">Query text to search items for</param>
    public async Task ReloadItems(string? query = null)
    {
        try
        {
            IsLoading = true;
            var request = new TQuery
            {
                Limit = EnableSearch ? Limit : null,
                Query = query
            };
            QueryModifier?.Invoke(request);
            var response = await Mediator.Send(request);
            if (InitialValue != null && !response.Results.Any(r => ItemsAreEqual(r, InitialValue)))
            {
                Items = response.Results.Concat(new[] { InitialValue }).ToArray();
            }
            else
            {
                Items = response.Results;
            }

            _preloaded = true;
        }
        finally
        {
            IsLoading = false;
            StateHasChanged();
        }
    }

    private void OnFocus()
    {
        if (!_preloaded)
            ReloadItems(null);
    }
}