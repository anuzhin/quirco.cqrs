﻿using Blazored.SessionStorage;
using Microsoft.Extensions.DependencyInjection;

namespace Quirco.Cqrs.Blazor;

public static class Extensions
{
    public static IServiceCollection AddQuircoBlazor(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddBlazoredSessionStorage();
        return serviceCollection;
    }
}