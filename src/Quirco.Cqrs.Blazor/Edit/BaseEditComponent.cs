﻿using System.Security.Claims;
using AntDesign;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quirco.Cqrs.Blazor.Navigation;
using Quirco.Cqrs.Blazor.QueryString;
using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Domain.Queries;
using Quirco.Cqrs.Infrastructure.Jobs;

namespace Quirco.Cqrs.Blazor.Edit;

public abstract class BaseEditComponent<TModel, TCreateUpdateModel> : OwningComponentBase
    where TCreateUpdateModel : class, new()
    where TModel : class
{
    protected bool IsLoading = true;
    protected TCreateUpdateModel? Model;
    protected TModel? ReadModel;

    [Parameter] public int Id { get; set; }

    [Inject] protected AuthenticationStateProvider AuthenticationStateProvider { get; set; }

    [Inject] public MessageService Messages { get; set; }

    public IJobsService Jobs { get; private set; }

    public IMediator Mediator { get; private set; }

    public IMapper Mapper { get; private set; }

    [Inject] public NavigationManager NavigationManager { get; set; }

    [Inject] public NavigationHistoryManager NavigationHistoryManager { get; set; }

    [Inject] public IAuthorizationService AuthorizationService { get; set; }

    [Inject] protected ILogger<BaseEditComponent<TModel, TCreateUpdateModel>> Logger { get; set; }

    protected ClaimsPrincipal User { get; private set; }

    public abstract string SuccessUrl { get; }

    public abstract string EntityName { get; }

    public abstract string? AddPermission { get; }

    public abstract string EditPermission { get; }

    public abstract string OpenPermission { get; }

    public virtual string DeletePermission { get; }
        
    protected bool IsSaving { get; set; }
        
    public string? CurrentActionPermission => Id == 0 ? AddPermission : EditPermission;

    protected bool HasErrors = false;

    protected override async Task OnInitializedAsync()
    {
        User = (await AuthenticationStateProvider.GetAuthenticationStateAsync()).User;
        Mediator = ScopedServices.GetRequiredService<IMediator>();
        Jobs = ScopedServices.GetRequiredService<IJobsService>();
        Mapper = ScopedServices.GetRequiredService<IMapper>();
    }

    protected abstract GetEntityByIdQuery<TModel> BuildGetQuery(int id);

    protected abstract IRequest<int> BuildCreateUpdateCommand();

    protected virtual async Task OnFinish()
    {
        try
        {
            IsSaving = true;
            if (Id == 0 && !(await AuthorizationService.AuthorizeAsync(User, AddPermission)).Succeeded ||
                Id != 0 && !(await AuthorizationService.AuthorizeAsync(User, EditPermission)).Succeeded)
            {
                throw new Exception("Отказано в доступе.");
            }

            var command = BuildCreateUpdateCommand();
            var result = await Mediator.Send(command);
            Id = result;
            StateHasChanged();

            await OnAfterSave(result);

            Messages.Success($"'{EntityName}' успешно сохранен(а)");
        }
        catch (Exception ex)
        {
            Logger.LogError(ex, "Ошибка при сохранении");

            Messages.Error("Ошибка при сохранении: " + ex.Message);

            if (ex.InnerException != null)
                Messages.Error("Подробности: " + ex.InnerException.Message);
        }
        finally
        {
            IsSaving = false;
        }
    }

    protected virtual async Task OnAfterSave(int id)
    {
        if (!string.IsNullOrEmpty(SuccessUrl))
            NavigationManager.NavigateTo(SuccessUrl);
        else if (await NavigationHistoryManager.CanGoBack())
            NavigationManager.NavigateTo((await NavigationHistoryManager.GetPreviousPage()).Url);
    }

    protected virtual async Task OnFinishFailed()
    {
        HasErrors = true;
        Messages.Error("Ошибка при сохранении");
    }

    protected virtual async Task OnDelete<DModel>()
    {
        try
        {
            var command = new DeleteCommand<DModel, int>(Id);
            await Mediator.Send(command);

            if (!string.IsNullOrEmpty(SuccessUrl))
                NavigationManager.NavigateTo(SuccessUrl);
            else if (await NavigationHistoryManager.CanGoBack())
                NavigationManager.NavigateTo((await NavigationHistoryManager.GetPreviousPage()).Url);

            Messages.Success($"'{EntityName}' успешно удален(а)");
        }
        catch (Exception ex)
        {
            Messages.Error(ex.Message);
        }
    }

    protected override async Task OnParametersSetAsync()
    {
        NavigationHistoryManager.AddPageToHistory(new NavigationHistoryRecord(EntityName, NavigationManager.Uri));
        await base.OnParametersSetAsync();
        try
        {
            if (Id != 0)
            {
                ReadModel = await Mediator.Send(BuildGetQuery(Id));
                Model = Mapper.Map<TCreateUpdateModel>(ReadModel);
            }
            else
            {
                Model = new TCreateUpdateModel();
            }
        }
        catch (Exception ex)
        {
            Logger.LogError(ex, "Ошибка при загрузке карточки");
            await Messages.Error(ex.Message);
        }
        finally
        {
            IsLoading = false;
        }
    }

    public override async Task SetParametersAsync(ParameterView parameters)
    {
        this.SetParametersFromQueryString(NavigationManager);
        await base.SetParametersAsync(parameters);
    }

}