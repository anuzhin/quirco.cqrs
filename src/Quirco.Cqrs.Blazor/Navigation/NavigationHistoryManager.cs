﻿using Blazored.SessionStorage;

namespace Quirco.Cqrs.Blazor.Navigation;

/// <summary>
/// Отвечает за стек навигации в приложении, позволяя отслеживать историю навигации, реализует возможность навигации "назад". 
/// </summary>
public class NavigationHistoryManager
{
    private readonly ISessionStorageService _sessionStorage;
    private Stack<NavigationHistoryRecord>? _navStack;

    public NavigationHistoryManager(ISessionStorageService sessionStorage)
    {
        _sessionStorage = sessionStorage;
    }

    private async Task<Stack<NavigationHistoryRecord>> GetNavStack()
    {
        _navStack ??= await _sessionStorage.GetItemAsync<Stack<NavigationHistoryRecord>>("nav-history") ?? new Stack<NavigationHistoryRecord>();

        return _navStack;
    }

    /// <summary>
    /// Добавляет страницу в стек навигации
    /// </summary>
    /// <param name="page"></param>
    public async Task AddPageToHistory(NavigationHistoryRecord page)
    {
        var navigationHistory = await GetNavStack();
        if (navigationHistory.TryPeek(out var current) && current == page)
            return;

        navigationHistory.Push(page);

        if (navigationHistory.Count > 10)
        {
            navigationHistory = new Stack<NavigationHistoryRecord>(navigationHistory.ToArray().Skip(5));
        }

        await SaveState(navigationHistory);
    }
        
    /// <summary>
    /// Заменяет последнюю страницу в стеке навигации
    /// </summary>
    /// <param name="page"></param>
    public async Task ReplaceLatestPage(NavigationHistoryRecord page)
    {
        var navigationHistory = await GetNavStack();
        if (navigationHistory == null) throw new ArgumentNullException(nameof(navigationHistory));
        navigationHistory.Pop();
        navigationHistory.Push(page);

        if (navigationHistory.Count > 10)
        {
            navigationHistory = new Stack<NavigationHistoryRecord>(navigationHistory.ToArray().Skip(5));
        }

        await SaveState(navigationHistory);
    }

    /// <summary>
    /// Указывает, возможна ли навигация "назад"
    /// </summary>
    /// <returns></returns>
    public async Task<bool> CanGoBack() => (await GetNavStack()).Count > 1;

    /// <summary>
    /// Возвращает параметры страницы для навигации назад
    /// </summary>
    /// <returns></returns>
    public async Task<NavigationHistoryRecord> GetPreviousPage()
    {
        var navigationHistory = await GetNavStack();
        navigationHistory.Pop(); // Выбрасываем текущую страницу
        var prevPage = navigationHistory.Pop();
        await SaveState(navigationHistory);
        return prevPage;
    }

    private async Task SaveState(Stack<NavigationHistoryRecord> history)
    {
        await _sessionStorage.SetItemAsync("nav-history", history);
    }
}