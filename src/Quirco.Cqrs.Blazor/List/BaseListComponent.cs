using AntDesign;
using MediatR;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Quirco.Cqrs.Blazor.Navigation;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Domain.Models;
using Quirco.Cqrs.Domain.Queries;
using Quirco.Cqrs.Mvc.StateProviders.Filter;

namespace Quirco.Cqrs.Blazor.List;

public abstract partial class BaseListComponent<TModel, TEntity, TQuery> : ComponentBase
    where TEntity : class, IHasId
    where TQuery : SearchQuery<TModel>, new()
{
    [Inject] protected IMediator Mediator { get; set; }

    [Inject] protected MessageService Messages { get; set; }

    [Inject] protected IFilterStateProvider FilterStateProvider { get; set; }

    [CascadingParameter] protected Task<AuthenticationState> AuthenticationStateTask { get; set; }

    [Inject] protected NavigationManager NavigationManager { get; set; }

    [Inject] NavigationHistoryManager NavigationHistoryManager { get; set; }

    protected int PageSize { get; set; } = 10;
    protected int PageIndex { get; set; } = 0;

    protected int Total { get; set; }

    public PagedResponse<TModel>? Items { get; protected set; }
    public bool IsLoading { get; protected set; }

    protected string? Query
    {
        get => Filter?.Query;
        set => Filter.Query = value;
    }

    protected TQuery Filter { get; private set; } = new();

    protected virtual async Task Reload()
    {
        try
        {
            IsLoading = true;
            Items = await Mediator.Send(Filter);
            Total = (int)Items.Count;
            await FilterStateProvider.PersistAsync(Filter);
            await ItemsLoaded();
        }
        finally
        {
            IsLoading = false;
        }
    }

    protected virtual Task ItemsLoaded()
    {
        return Task.CompletedTask;
    }

    protected virtual TQuery BuildDefaultQuery() => new()
    {
        Offset = PageIndex == 0 ? 0 : (PageIndex - 1) * PageSize,
        Limit = PageSize
    };

    protected async Task DeleteItem(int itemId)
    {
        try
        {
            await Mediator.Send(new DeleteCommand<TEntity, int>(itemId));
            await Reload();
            await Messages.Success("Успешно удалено");
        }
        catch (FluentValidation.ValidationException e)
        {
            await Messages.Error(string.Join("\n", e.Errors));
        }
        catch (Exception e)
        {
            await Messages.Error("Ошибка при удалении: " + e.Message);
        }
    }

    protected override async Task OnParametersSetAsync()
    {
        NavigationHistoryManager.AddPageToHistory(new NavigationHistoryRecord("", NavigationManager.Uri));
        try
        {
            IsLoading = true;
            await base.OnParametersSetAsync();
            Filter = BuildDefaultQuery();
            await FilterStateProvider.RestoreAsync(Filter);
            if (Filter.Limit != null && Filter.Limit != 0)
            {
                PageIndex = (int)(Filter.Offset / Filter.Limit) + 1;
                PageSize = Filter.Limit ?? 10;
            }

            await OnPrepareFiltersAsync();
            await Reload();
        }
        finally
        {
            IsLoading = false;
        }

        await Task.Delay(50);
    }

    protected virtual Task OnPrepareFiltersAsync() => Task.CompletedTask;

    protected async Task PageChanged(PaginationEventArgs arg)
    {
        PageIndex = arg.Page;
        PageSize = arg.PageSize;
        Filter.Offset = (arg.Page - 1) * arg.PageSize;
        Filter.Limit = arg.PageSize;
        await FilterStateProvider.PersistAsync(Filter);
        await Reload();
    }

    protected async Task PageSizeChanged(int pageSize)
    {
        await PageChanged(new PaginationEventArgs(PageIndex, pageSize));
    }
}