namespace Quirco.Cqrs.Blazor.Interface;

public class GridSizingConstants
{
    public static readonly Dictionary<string, int> Gutter = new()
    {
        ["xs"] = 8,
        ["sm"] = 16,
        ["md"] = 24,
        ["lg"] = 32,
        ["xl"] = 48,
        ["xxl"] = 64
    };
}