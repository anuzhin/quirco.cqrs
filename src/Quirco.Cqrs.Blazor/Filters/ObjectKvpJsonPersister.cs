﻿using System.Reflection;
using Newtonsoft.Json;

namespace Quirco.Cqrs.Blazor.Filters;

public class ObjectKvpJsonPersister
{
    /// <summary>
    /// Convert object to key-value pair, where key is property name, value - json-serialized value
    /// </summary>
    /// <param name="obj">Object to persist</param>
    /// <typeparam name="T">Type of object to persist</typeparam>
    /// <returns>IEnumerable of (string,string) pairs</returns>
    public static IEnumerable<KeyValuePair<string, string?>> Persist<T>(T obj)
    {
        foreach (var property in typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public))
        {
            var value = property.GetValue(obj);
            if (value != null)
                yield return new KeyValuePair<string, string?>(property.Name, JsonConvert.SerializeObject(value));
        }
    }
    
    /// <summary>
    /// Restores state of object from key-value pairs of property name + property value serialized to JSON
    /// </summary>
    /// <param name="obj">Instance to fill with restored data</param>
    /// <param name="keyValues">Data parts (key-value pairs)</param>
    /// <typeparam name="T">Type of instance to restore</typeparam>
    /// <returns>Same instance of object from input</returns>
    public static T Restore<T>(T obj, IEnumerable<KeyValuePair<string, string?>> keyValues)
    {
        foreach (var (name, value) in keyValues.Where(v=>v.Value != null))
        {
            var property = typeof(T).GetProperty(name, BindingFlags.Public | BindingFlags.Instance);
            if (property == null)
                continue;
            property.SetValue(obj, JsonConvert.DeserializeObject(value, property.PropertyType));
        }

        return obj;
    }
}