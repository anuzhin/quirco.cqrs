﻿using System.Net;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;
using Quirco.Cqrs.Mvc.StateProviders.Filter;

namespace Quirco.Cqrs.Blazor.Filters;

public class QueryStringStateProvider : IFilterStateProvider
{
    private readonly NavigationManager _navigationManager;

    public QueryStringStateProvider(NavigationManager navigationManager)
    {
        _navigationManager = navigationManager;
    }

    public Task PersistAsync<TState>(TState state)
    {
        var url = QueryHelpers.AddQueryString(_navigationManager.Uri,
            ObjectKvpJsonPersister.Persist(state).Select(kv => new KeyValuePair<string, StringValues>(kv.Key, WebUtility.UrlEncode(kv.Value))));
        _navigationManager.NavigateTo(url, false);
        return Task.CompletedTask;
    }

    public Task<TState> RestoreAsync<TState>(TState state)
    {
        var uri = _navigationManager.ToAbsoluteUri(_navigationManager.Uri);
        var query = QueryHelpers.ParseQuery(uri.Query);
        ObjectKvpJsonPersister.Restore(state, query.Select(kv => new KeyValuePair<string, string?>(kv.Key, kv.Value.ToString())));
        return Task.FromResult(state);
    }
}