using Quirco.Cqrs.Infrastructure.DateTime;

namespace Quirco.Cqrs.Tests
{
    public class CurrentMockDateTimeProvider : ICurrentDateTimeProvider
    {
        public DateTime Now { get; set; }

        public CurrentMockDateTimeProvider()
        {
            Now = DateTime.Now;
        }
    }
}