using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Quirco.Cqrs.Tests;

public abstract class BaseIntegrationTest<TStartup, TContext> : IDisposable
    where TStartup : class
    where TContext : DbContext
{
    protected readonly IWebHost WebHost;

    protected BaseIntegrationTest()
    {
        WebHost = new WebHostBuilder()
            .UseEnvironment("Test")
            .ConfigureAppConfiguration((ctx, e) =>
            {
                e.AddJsonFile("appsettings.json")
                    .AddEnvironmentVariables();
            })
            .UseStartup<TStartup>()
            .Build();
        var ctx = Context;
    }

    private TContext? _context;
    protected TContext Context => _context ??= BuildContext();

    private TContext BuildContext()
    {
        var context = WebHost.Services.GetRequiredService<TContext>();
        InitializeDbContext(context);
        context.SaveChanges();
        return context;
    }
        
    private IDbContextFactory<TContext>? _contextFactory;
    protected IDbContextFactory<TContext> ContextFactory => _contextFactory ??= BuildContextFactory();

    private IDbContextFactory<TContext> BuildContextFactory()
    {
        Context.SaveChanges();
        return WebHost.Services.GetRequiredService<IDbContextFactory<TContext>>();
    }

    public TService GetService<TService>()
    {
        return WebHost.Services.GetRequiredService<TService>();
    }

    protected abstract void InitializeDbContext(TContext context);

    public void Dispose()
    {
        Context?.Dispose();
        WebHost?.Dispose();
    }
}