using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Quirco.Cqrs.Infrastructure.Jobs;

namespace Quirco.Cqrs.Tests.Startups;

public static class InMemoryTestStartup
{
    public static void AddTestServices<TContext>(this IServiceCollection services) where TContext : DbContext
    {
        var descriptor = services.SingleOrDefault(
            d => d.ServiceType ==
                 typeof(DbContextOptions<TContext>));

        if (descriptor != null)
        {
            services.Remove(descriptor);
        }

        services.AddEntityFrameworkInMemoryDatabase();
            
        // Add ApplicationDbContext using an in-memory database for testing.
        services.AddDbContextFactory<TContext>(options =>
        {
            options.UseInMemoryDatabase(Guid.NewGuid().ToString());
            options.EnableDetailedErrors();
            options.EnableSensitiveDataLogging();
        }, ServiceLifetime.Scoped);
        services.AddScoped<TContext>(p => p.GetRequiredService<IDbContextFactory<TContext>>().CreateDbContext());

        services.AddSingleton(Mock.Of<IJobsService>());
    }
}