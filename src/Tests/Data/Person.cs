﻿using Quirco.Cqrs.DataAccess.Models;

namespace Tests.Data;

public class Person : Entity, IHasDateDeleted, IHasUserCreated
{
    public string Name { get; set; }
        
    public int Age { get; set; }
        
    public DateTime? DateDeleted { get; set; }
        
    public int UserCreatedId { get; set; }
        
    public ICollection<Car> Cars { get; set; }
}