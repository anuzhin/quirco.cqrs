﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.DataAccess.Auditable;

namespace Tests.Data;

public class TestDbContext : DbContext
{
    public DbSet<Person> Persons { get; set; }
    public DbSet<Car> Cars { get; set; }

    private readonly DbContextAuditor Auditor;
        
    public TestDbContext(
        IMediator mediator, 
        DbContextOptions<TestDbContext> options) : base(options)
    {
        Auditor = new DbContextAuditor(mediator, this);
        Auditor.AddAuditableEntity<Person>();
    }
        
    public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
    {
        return await Auditor.SaveChanges(ct => base.SaveChangesAsync(ct), () => 1, cancellationToken);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Person>().HasMany(p => p.Cars).WithOne(c => c.Owner);
        base.OnModelCreating(modelBuilder);
    }
}