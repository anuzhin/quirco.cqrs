﻿using Quirco.Cqrs.DataAccess.Models;

namespace Tests.Data;

public class Car : IHasId
{
    public int Id { get; set; }
    
    public string Model { get; set; }
    
    public int OwnerId { get; set; }
    
    public Person Owner { get; set; }
}