﻿using AutoMapper;
using Quirco.Cqrs.Tests;
using Tests.Data;
using Tests.Domain.Profiles;

namespace Tests.Base;

public class BaseCqrsTest : BaseIntegrationTest<CqrsTestStartup, TestDbContext>
{
    protected override void InitializeDbContext(TestDbContext context)
    {
        context.SaveChanges();
    }

    protected Mapper Mapper => new(new MapperConfiguration(cfg =>
        cfg.AddProfile<DomainProfile>()
    ));
}