﻿using System.ComponentModel;
using Quirco.Security.Attributes;

namespace Tests.Security;

[PermissionsMetadata("TestModule")]
public static class PropertyBasedPermissions
{
    [DisplayName("Person")]
    public static class Person
    {
        public static string Add { get; private set; }
        
        public static string View { get; private set; }
        
        public static string Edit { get; private set; }
        
        public static string Delete { get; private set; }
    }
}