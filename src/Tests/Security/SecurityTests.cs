﻿using FluentAssertions;
using Quirco.Security.Permission;
using Xunit;

namespace Tests.Security;

public class SecurityTests
{
    [Fact]
    public void TestListing()
    {
        // Arrange
        var provider = new AssemblyScanPermissionsProvider(GetType().Assembly);

        // Act
        var permissions = provider.GetAllPermissions().ToList();

        // Assert
        permissions.Should().HaveCount(2);
        permissions[0].ModuleName.Should().Be("TestModule");
        permissions[0].EntityName.Should().Be("Person");
        permissions[0].Permissions.Should().HaveCount(4);
        permissions[0].Permissions.Should().BeEquivalentTo(new[]
        {
            "Security.Person.Add",
            "Security.Person.View",
            "Security.Person.Edit",
            "Security.Person.Delete"
        });
        permissions[0].ReadablePermissions.Should().HaveCount(4);
        permissions[0].ReadablePermissions.Should().BeEquivalentTo(new[]
        {
            "TestModule.Person.Add",
            "TestModule.Person.View",
            "TestModule.Person.Edit",
            "TestModule.Person.Delete"
        });
        
        permissions[1].ModuleName.Should().Be("TestModule");
        permissions[1].EntityName.Should().Be("Person");
        permissions[1].Permissions.Should().HaveCount(4);
        permissions[1].Permissions.Should().BeEquivalentTo(new[]
        {
            "Tests.Security.Person.Add",
            "Tests.Security.Person.View",
            "Tests.Security.Person.Edit",
            "Tests.Security.Person.Delete"
        });
        permissions[1].ReadablePermissions.Should().HaveCount(4);
        permissions[1].ReadablePermissions.Should().BeEquivalentTo(new[]
        {
            "TestModule.Person.Add",
            "TestModule.Person.View",
            "TestModule.Person.Edit",
            "TestModule.Person.Delete"
        });
    }
}