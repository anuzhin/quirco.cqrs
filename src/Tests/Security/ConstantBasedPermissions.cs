﻿using System.ComponentModel;
using Quirco.Security;
using Quirco.Security.Attributes;

namespace Tests.Security;

[PermissionsMetadata("TestModule")]
public static class ConstantBasedPermissions
{
    [DisplayName("Person")]
    public static class Person
    {
        [RequireRoles(BuiltInRoles.Admin)]
        public const string Add = "Security.Person.Add";
        
        public const string View = "Security.Person.View";
        
        public const string Edit = "Security.Person.Edit";
        
        public const string Delete = "Security.Person.Delete";
    }
}