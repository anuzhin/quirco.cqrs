﻿using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Tests.Data;
using Xunit;

namespace Tests.DataAccess;

public class DbContextAuditorTests
{
    private readonly ServiceProvider _serviceProvider;

    public DbContextAuditorTests()
    {
        _serviceProvider = new ServiceCollection()
            .AddDbContextFactory<TestDbContext>(o => o.UseInMemoryDatabase(Guid.NewGuid().ToString()))
            .AddMediatR(GetType().Assembly)
            .BuildServiceProvider();
    }

    [Fact]
    public async Task TestInsertion()
    {
        // Arrange
        var contextFactory = _serviceProvider.GetRequiredService<IDbContextFactory<TestDbContext>>();
        var ctx = contextFactory.CreateDbContext();

        // Act
        ctx.Persons.Add(new Person
        {
            Name = "Sergio"
        });
        await ctx.SaveChangesAsync();
        var persons = await ctx.Persons.ToListAsync();

        // Assert
        persons.Should().NotBeEmpty();
        persons.Should().HaveCount(1);
        var person = persons.First();
        person.Id.Should().BeGreaterThan(0);
        person.UserCreatedId.Should().Be(1);
    }
    
    [Fact]
    public async Task TestDeletion()
    {
        // Arrange
        var contextFactory = _serviceProvider.GetRequiredService<IDbContextFactory<TestDbContext>>();
        var ctx = contextFactory.CreateDbContext();
        ctx.Persons.Add(new Person
        {
            Name = "Sergio"
        });
        await ctx.SaveChangesAsync();

        // Act
        var person = await ctx.Persons.FirstAsync();
        ctx.Persons.Remove(person);
        await ctx.SaveChangesAsync();
        person = await ctx.Persons.FirstOrDefaultAsync();

        // Assert
        person.Should().NotBeNull();
        person.DateDeleted.Should().NotBeNull();
    }
}