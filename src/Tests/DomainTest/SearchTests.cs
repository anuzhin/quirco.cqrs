﻿using FluentAssertions;
using Tests.Base;
using Tests.Data;
using Tests.Domain.Queries;
using Xunit;

namespace Tests.DomainTest;

public class SearchTests : BaseCqrsTest
{
    [Fact(DisplayName = "Test for search of all records")]
    public async Task PagingTest1()
    {
        // Arrange
        await Context.Persons.AddRangeAsync(
            new Person
            {
                Name = "Sergey",
                Age = 39
            },
            new Person
            {
                Name = "Julia",
                Age = 37
            },
            new Person
            {
                Name = "Darya",
                Age = 9
            }
        );
            
            
        // Act
        var handler = new SearchPersonsQueryHandler(Mapper, ContextFactory, null);
        var result = await handler.Handle(new SearchPersonsQuery(), CancellationToken.None);
            
        // Assert
        result.Count.Should().Be(3);
        result.Results.Should().HaveCount(3);
    }

    [Fact(DisplayName = "Test for search paged result")]
    public async Task PagingTest2()
    {
        // Arrange
        await Context.Persons.AddRangeAsync(
            new Person
            {
                Name = "Sergey",
                Age = 39
            },
            new Person
            {
                Name = "Julia",
                Age = 37
            },
            new Person
            {
                Name = "Darya",
                Age = 9
            }
        );
            
            
        // Act
        var handler = new SearchPersonsQueryHandler(Mapper, ContextFactory, null);
        var result = await handler.Handle(new SearchPersonsQuery
        {
            Offset = 1,
            Limit = 1
        }, CancellationToken.None);
            
        // Assert
        result.Count.Should().Be(3);
        result.Results.Should().HaveCount(1);
        result.Results[0].Name.Should().Be("Julia");
    }
        
        
}