﻿using FluentAssertions;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Cqrs.Domain.Commands;
using Tests.Data;
using Tests.Domain.Commands;
using Xunit;

namespace Tests.DomainTest;

public class DeleteCommandTest
{
    [Fact]
    public async Task DeleteTest()
    {
        // Arrange
        var serviceProvider = new ServiceCollection()
            .AddDbContext<TestDbContext>(c => c.UseInMemoryDatabase(Guid.NewGuid().ToString()))
            .AddMediatR(typeof(DeleteCommand<>).Assembly, typeof(DeleteCarCommandHandler).Assembly)
            .AddFluentValidation(c=>c.RegisterValidatorsFromAssembly(typeof(DeleteCommand<>).Assembly))
            .BuildServiceProvider();

        await using var context = serviceProvider.GetRequiredService<TestDbContext>();
        var car = new Car { Model = "Ford" };
        context.Persons.Add(new Person
        {
            Name = "Test",
            Cars = new List<Car>
            {
                car
            }
        });
        await context.SaveChangesAsync();

        // Act
        var mediator = serviceProvider.GetRequiredService<IMediator>();
        await mediator.Send(new DeleteCommand<Car, int>(car.Id));

        // Assert
        context.Cars.Count().Should().Be(0, "No cars in database after deletion");
    }

    [Fact]
    public async Task TestSoftDeletion()
    {
        // Arrange
        var serviceProvider = new ServiceCollection()
            .AddDbContext<TestDbContext>(c => c.UseInMemoryDatabase(Guid.NewGuid().ToString()))
            .AddMediatR(typeof(DeleteCommand<>).Assembly, typeof(DeleteCarCommandHandler).Assembly)
            .AddFluentValidation(c=>c.RegisterValidatorsFromAssembly(typeof(DeleteCommand<>).Assembly))
            .BuildServiceProvider();

        await using var context = serviceProvider.GetRequiredService<TestDbContext>();
        var person = new Person
        {
            Name = "Test"
        };
        context.Persons.Add(person);
        await context.SaveChangesAsync();

        // Act
        var mediator = serviceProvider.GetRequiredService<IMediator>();
        await mediator.Send(new DeleteCommand<Person, int>(person.Id));

        // Assert
        var dbPerson = await context.Persons.FirstOrDefaultAsync();
        dbPerson.Should().NotBeNull();
        dbPerson.DateDeleted.Should().NotBeNull();
    }
}