﻿using Quirco.Cqrs.Domain.Commands;
using Quirco.Cqrs.Domain.Security;
using Tests.Data;

namespace Tests.Domain.Commands;

public class DeleteCarCommandHandler: DeleteCommandHandler<TestDbContext, Car>
{
    public DeleteCarCommandHandler(TestDbContext context, IEnumerable<IPermissionEvaluator<Car>>? evaluators) : base(context, evaluators)
    {
    }
}