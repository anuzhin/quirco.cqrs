﻿using Quirco.Cqrs.DataAccess.Models;

namespace Tests.Domain.Models;

public class PersonSlimModel : IHasId
{
    public int Id { get; set; }
    public string Name { get; set; }
}