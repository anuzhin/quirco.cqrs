﻿using Quirco.Cqrs.Domain.Queries;
using Tests.Domain.Models;

namespace Tests.Domain.Queries;

public class GetPersonByIdQuery : GetEntityByIdQuery<PersonModel>
{
    public GetPersonByIdQuery(int id) : base(id)
    {
    }
}