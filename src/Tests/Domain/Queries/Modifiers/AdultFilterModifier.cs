﻿using Quirco.Cqrs.Domain.Filters;
using Tests.Data;

namespace Tests.Domain.Queries.Modifiers;

public class AdultFilterModifier : IQueryableModifier<Person>
{
    public async Task<IQueryable<Person>> Modify(IQueryable<Person> query)
    {
        await Task.Run(() => query = query.Where(p => p.Age >= 18));
        return query;
    }
}