﻿using FluentAssertions;
using Quirco.Cqrs.Blazor.Filters;
using Xunit;

namespace Tests.Blazor;

public class ObjctKvpPersisterTest
{
    public class TestObject
    {
        public int Id { get; set; }
        
        public DateTime? DateTime { get; set; }
        
        public string[] SomeValues { get; set; }
    }

    
    [Fact]
    public void TestPersistence()
    {
        // Arrange
        var obj = new TestObject
        {
            Id = 1,
            DateTime = DateTime.Today,
            SomeValues = new[] { "s1", "s2", "s 3" }
        };

        // Act
        var kvp = ObjectKvpJsonPersister.Persist(obj).ToArray();

        // Assert
        kvp.Should().BeEquivalentTo(new[]
        {
            new KeyValuePair<string, string?>("Id", "1"),
            new KeyValuePair<string, string?>("DateTime", "\"" + DateTime.Today.ToString("yyyy-MM-ddTHH:mm:sszzz") + "\""),
            new KeyValuePair<string, string?>("SomeValues", "[\"s1\",\"s2\",\"s 3\"]"),
        });
    }
    
    [Fact]
    public void TestRestore()
    {
        // Arrange
        var restored = new TestObject();
        
        // Act
        restored = ObjectKvpJsonPersister.Restore(restored, new[]
        {
            new KeyValuePair<string, string?>("Id", "1"),
            new KeyValuePair<string, string?>("DateTime", "\"" + DateTime.Today.ToString("yyyy-MM-ddTHH:mm:sszzz") + "\""),
            new KeyValuePair<string, string?>("SomeValues", "[\"s1\",\"s2\",\"s 3\"]"),
        });

        // Assert
        restored.Should().BeEquivalentTo(new TestObject
        {
            Id = 1,
            DateTime = DateTime.Today,
            SomeValues = new[] { "s1", "s2", "s 3" }
        });
    }
}