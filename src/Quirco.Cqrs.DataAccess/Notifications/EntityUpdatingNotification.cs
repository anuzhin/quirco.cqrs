namespace Quirco.Cqrs.DataAccess.Notifications;

/// <summary>
/// Notification about entity changing event.
/// </summary>
/// <typeparam name="T"></typeparam>
public class EntityUpdatingNotification<T> : IEntityUpdatingNotification<T>
{
    /// <summary>
    /// Entity.
    /// </summary>
    public T Entity { get; }
        
    /// <summary>
    /// Action type.
    /// </summary>
    public EntityAction Action { get; }

    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="EntityUpdatingNotification{T}"/>.
    /// </summary>
    /// <param name="entity"></param>
    /// <param name="action"></param>
    public EntityUpdatingNotification(T entity, EntityAction action)
    {
        Entity = entity;
        Action = action;
    }
        
    /// <summary>
    /// Creates notification about added entity.
    /// </summary>
    /// <param name="entity">Added entity.</param>
    /// <returns></returns>
    public static EntityUpdatingNotification<T> CreateAdded(T entity) => new (entity, EntityAction.Added);
        
    /// <summary>
    /// Creates notification about modified entity.
    /// </summary>
    /// <param name="entity">Modified entity.</param>
    /// <returns></returns>
    public static EntityUpdatingNotification<T> CreateModified(T entity) => new (entity, EntityAction.Modified);
        
    /// <summary>
    /// Creates notification about deleted entity.
    /// </summary>
    /// <param name="entity">Deleted entity.</param>
    /// <returns></returns>
    public static EntityUpdatingNotification<T> CreateDeleted(T entity) => new (entity, EntityAction.Deleted);
}