﻿using System.Reflection;
using System.Transactions;
using MediatR;
using Microsoft.Extensions.Logging;
using Quirco.Cqrs.Domain.Behaviors;

namespace Quirco.Cqrs.DataAccess.Behaviors;

/// <summary>
/// Обеспечивает выполнение команд, отмеченных атрибутом <see cref="TransactionAttribute"/> в рамках транзакции
/// </summary>
/// <typeparam name="TRequest"></typeparam>
/// <typeparam name="TResponse"></typeparam>
public class TransactionPipelineBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
{
    private readonly ILogger<TransactionPipelineBehavior<TRequest, TResponse>> _logger;

    /// <summary>
    /// Initializes a new instance of the <see cref="TransactionPipelineBehavior{TRequest, TResponse}"/>.
    /// </summary>
    /// <param name="logger"></param>
    public TransactionPipelineBehavior(ILogger<TransactionPipelineBehavior<TRequest,TResponse>> logger)
    {
        _logger = logger;
    }

    /// <inheritdoc />
    public async Task<TResponse> Handle(
        TRequest request,
        CancellationToken cancellationToken,
        RequestHandlerDelegate<TResponse> next)
    {
        var transaction = typeof(TRequest).GetCustomAttribute<TransactionAttribute>();

        if (transaction == null)
        {
            return await next();
        }

        var options = GetTransactionOptions(transaction);
        _logger.LogInformation("Request {Request} is running inside transaction with isolation level {Level}", typeof(TRequest).FullName, options.IsolationLevel);
        using var tran = new TransactionScope(TransactionScopeOption.Required, options, TransactionScopeAsyncFlowOption.Enabled);
        var result = await next();
        tran.Complete();
        return result;
    }

    private TransactionOptions GetTransactionOptions(TransactionAttribute transaction)
    {
        var opts = new TransactionOptions
        {
            IsolationLevel = transaction.IsolationLevel
        };

        return opts;
    }
}