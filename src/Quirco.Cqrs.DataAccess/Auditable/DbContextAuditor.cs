using MediatR;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.DataAccess.Notifications;


namespace Quirco.Cqrs.DataAccess.Auditable;

/// <summary>
/// Binds DbContext events and IMediator, publishes added/updated/deleted events to mediator event pipeline.
/// Use <see cref="AddAuditableEntity{T}" /> method to add types you whant listen audit events of.
/// </summary>
public class DbContextAuditor
{
    private readonly IMediator _mediator;
    private readonly DbContext _context;
    private readonly HashSet<Type> _auditableTypes;

    public DbContextAuditor(IMediator mediator, DbContext context)
    {
        _mediator = mediator;
        _context = context;
        _auditableTypes = new HashSet<Type>();
    }

    /// <summary>
    /// Wrapper over default DbContext.SaveChanges method, making all the stuff needed to publish events
    /// </summary>
    /// <param name="saveFunction">Default DbContext.SaveChanges implementation</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Count of entries changed in DB</returns>
    public Task<int> SaveChanges(Func<CancellationToken, Task<int>> saveFunction, CancellationToken cancellationToken)
    {
        return SaveChanges<int>(saveFunction, null, cancellationToken);
    }

    /// <summary>
    /// Wrapper over default DbContext.SaveChanges method, making all the stuff needed to publish events
    /// </summary>
    /// <param name="saveFunction">Default DbContext.SaveChanges implementation</param>
    /// <param name="currentUserIdAccessor">Expression to provide ID of user. Can be used for automatic filling of <see cref="IHasUserCreated"/> entities</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Count of entries changed in DB</returns>
    public async Task<int> SaveChanges<TUserId>(Func<CancellationToken, Task<int>> saveFunction,
        Func<TUserId>? currentUserIdAccessor, CancellationToken cancellationToken)
    {
        var modifiedEntries = _context.ChangeTracker
            .Entries()
            .Where(x => x.State != EntityState.Unchanged && _auditableTypes.Contains(x.Entity.GetType()))
            .Select(x => new AuditableEntry(x.State, x)).ToArray();

        if (modifiedEntries.Any())
        {
            UpdateDates(modifiedEntries);
            if (currentUserIdAccessor != null)
            {
                UpdateUsers(modifiedEntries, currentUserIdAccessor);
            }

            await FireEntityEvents(modifiedEntries, typeof(EntityUpdatingNotification<>), cancellationToken);
        }

        var res = await saveFunction(cancellationToken);

        if (res > 0 && modifiedEntries.Length > 0)
        {
            await FireEntityEvents(modifiedEntries, typeof(EntityUpdatedNotification<>), cancellationToken);
        }

        return res;
    }

    private void UpdateUsers<TUserId>(IEnumerable<AuditableEntry> modifiedEntries, Func<TUserId> currentUserIdAccessor)
    {
        var userId = default(TUserId);
        var userInitialized = false;
        foreach (var addedEntries in modifiedEntries.Where(e => e.State == EntityState.Added)
                     .Select(e => e.Entity)
                     .OfType<IHasUserCreated<TUserId>>())
        {
            if (!userInitialized)
            {
                userId = currentUserIdAccessor();
                userInitialized = true;
            }

            addedEntries.UserCreatedId = userId;
        }
    }

    protected virtual void UpdateDates(IEnumerable<AuditableEntry> entries)
    {
        //_context.Ser
        foreach (var entry in entries)
        {
            if (entry.State == EntityState.Added && entry.Entity is IHasDateCreated newEntry)
            {
                newEntry.DateCreated = DateTime.UtcNow;
            }

            if (entry.State is EntityState.Modified or EntityState.Added && entry.Entity is IHasDateModified modifiedEntry)
            {
                modifiedEntry.DateModified = DateTime.UtcNow;
            }

            if (entry.State == EntityState.Deleted && entry.Entity is IHasDateDeleted deletedEntry)
            {
                deletedEntry.DateDeleted = DateTime.UtcNow;
                entry.EntityEntry.State = EntityState.Modified;
            }
        }
    }

    /// <summary>
    /// Adds entity type to be tracked with auditor. All entities of this type, added to context, will fire create/update/delete events.
    /// </summary>
    /// <typeparam name="T">Type of entity to be tracked</typeparam>
    public DbContextAuditor AddAuditableEntity<T>()
    {
        _auditableTypes.Add(typeof(T));
        return this;
    }

    /// <summary>
    /// Fires entity changed events to mediator pipeline
    /// </summary>
    /// <param name="modifiedEntries">List of modified entities</param>
    /// <param name="eventType">Type of event to fire (updating or updated)</param>
    /// <param name="cancellationToken"></param>
    protected virtual async Task FireEntityEvents(IEnumerable<AuditableEntry> modifiedEntries, Type eventType, CancellationToken cancellationToken)
    {
        foreach (var entry in modifiedEntries)
        {
            var genericType = eventType.MakeGenericType(entry.Entity.GetType());
            var evt = Activator.CreateInstance(genericType, entry.Entity, MapEfState(entry.State));
            if (evt != null)
            {
                await _mediator.Publish(evt, cancellationToken);
            }
        }
    }

    private static EntityAction MapEfState(EntityState state)
    {
        return state switch
        {
            EntityState.Added => EntityAction.Added,
            EntityState.Deleted => EntityAction.Deleted,
            EntityState.Modified => EntityAction.Modified,
            _ => throw new NotSupportedException()
        };
    }
}