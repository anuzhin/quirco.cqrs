﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;

namespace Quirco.Cqrs.DataAccess.Converters;

public class EnumToStringConverter<TEnum> : StringEnumConverter<TEnum, string, TEnum>
    where TEnum : struct 
{

    public EnumToStringConverter(ConverterMappingHints mappingHints = null)
        : base(
            sourceEnum => sourceEnum.ToString().ToUpper(),
            ToEnum(),
            mappingHints)
    { }
        

    /// <summary>
    ///     A <see cref="ValueConverterInfo" /> for the default use of this converter.
    /// </summary>
    public static ValueConverterInfo DefaultInfo { get; }
        = new (typeof(TEnum), typeof(string), i => new EnumToStringConverter<TEnum>(i.MappingHints));
}