﻿using Microsoft.EntityFrameworkCore;

namespace Quirco.Cqrs.DataAccess.DataContext;

/// <summary>
/// Extensions for the <see cref="ModelBuilder"/>.
/// </summary>
public static class ModelBuilderExtensions
{
    /// <summary>
    /// Отключить глобально каскадное удаление.
    /// </summary>
    /// <param name="modelBuilder"></param>
    /// <returns></returns>
    public static ModelBuilder DisableCascadeDeleteGlobally(this ModelBuilder modelBuilder)
    {
        var cascadeForeignKeys = modelBuilder.Model.GetEntityTypes()
            .SelectMany(t => t.GetForeignKeys())
            .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

        foreach (var fk in cascadeForeignKeys)
        {
            fk.DeleteBehavior = DeleteBehavior.Restrict;
        }

        return modelBuilder;
    }
}