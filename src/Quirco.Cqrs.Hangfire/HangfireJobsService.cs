using System.Linq.Expressions;
using Hangfire;
using Quirco.Cqrs.Infrastructure.Jobs;

namespace Quirco.Cqrs.Hangfire;

public class HangfireJobsService : IJobsService
{
    public Task<string> Enqueue<T>(Expression<Func<T, Task>> methodCall) => Task.FromResult(BackgroundJob.Enqueue(methodCall));

    public Task Schedule<T>(string id, Expression<Func<T, Task>> methodCall, string cronExpression)
    {
        RecurringJob.AddOrUpdate(methodCall, cronExpression, TimeZoneInfo.Local);
        return Task.CompletedTask;
    }

    public Task<string> Schedule<T>(Expression<Func<T, Task>> methodCall, TimeSpan delay) =>
        Task.FromResult(BackgroundJob.Schedule(methodCall, delay));
}