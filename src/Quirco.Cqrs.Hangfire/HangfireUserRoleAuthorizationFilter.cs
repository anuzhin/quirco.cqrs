﻿using Hangfire.Dashboard;

namespace Quirco.Cqrs.Hangfire;

public class HangfireUserRoleAuthorizationFilter : IDashboardAuthorizationFilter
{
    private readonly string _allowRole;

    public HangfireUserRoleAuthorizationFilter(string allowRole)
    {
        _allowRole = allowRole;
    }

    public bool Authorize(DashboardContext context)
    {
        var httpContext = context.GetHttpContext();
        return httpContext?.User.IsInRole(_allowRole) == true;
    }
}