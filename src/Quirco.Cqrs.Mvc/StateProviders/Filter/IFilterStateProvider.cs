﻿namespace Quirco.Cqrs.Mvc.StateProviders.Filter;

public interface IFilterStateProvider
{
    Task PersistAsync<TState>(TState state);
    Task<TState> RestoreAsync<TState>(TState state);
}