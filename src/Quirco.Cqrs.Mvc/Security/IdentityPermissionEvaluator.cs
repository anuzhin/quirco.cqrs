using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Authorization;
using Quirco.Cqrs.Domain.Security;

namespace Quirco.Cqrs.Mvc.Security;

public abstract class IdentityPermissionEvaluator<TEntity> : IPermissionEvaluator<TEntity> where TEntity : class
{
    private readonly IAuthorizationService _authorizationService;
    private readonly IPrincipal _principal;

    protected IdentityPermissionEvaluator(
        IAuthorizationService authorizationService,
        IPrincipal principal)
    {
        _authorizationService = authorizationService;
        _principal = principal;
    }

    protected abstract string AddPermission { get; }
    protected abstract string EditPermission { get; }
    protected abstract string DeletePermission { get; }

    protected virtual async Task<bool> AuthorizeAsync(TEntity entity, string permission, CancellationToken cancellationToken)
    {
        if (_principal is ClaimsPrincipal user)
        {
            return (await _authorizationService.AuthorizeAsync(user, permission)).Succeeded;
        }

        return false;
    }

    public async Task<bool> CanAdd(TEntity entity, CancellationToken cancellationToken) =>
        await AuthorizeAsync(entity, AddPermission, cancellationToken);

    public async Task<bool> CanUpdate(TEntity entity, CancellationToken cancellationToken) =>
        await AuthorizeAsync(entity, EditPermission, cancellationToken);

    public async Task<bool> CanDelete(TEntity entity, CancellationToken cancellationToken) =>
        await AuthorizeAsync(entity, DeletePermission, cancellationToken);
}