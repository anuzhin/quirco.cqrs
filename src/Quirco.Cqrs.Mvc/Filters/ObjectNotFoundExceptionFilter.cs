using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Quirco.Cqrs.Domain.Exceptions;

namespace Quirco.Cqrs.Mvc.Filters;

/// <summary>
/// Отлавливает исключение о ненайденном объекте.
/// </summary>
public class ObjectNotFoundExceptionFilter<T> : ExceptionFilter<ObjectNotFoundException<T>>
{
    /// <inheritdoc />
    protected override void Handle(ObjectNotFoundException<T> exception, ExceptionContext context)
    {
        context.Result = new NotFoundResult();
    }
}