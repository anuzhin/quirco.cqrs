﻿namespace Quirco.Security;

public class BuiltInRoles
{
    public const string Admin = nameof(Admin);
    public const string Employee = nameof(Employee);
}