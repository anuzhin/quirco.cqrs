﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Quirco.Security.Permission;

namespace Quirco.Security.Extensions;

public static class PolicyServicesExtensions
{
    /// <summary>
    /// Configures asp.net authorization for list of claims and policies
    /// </summary>
    /// <param name="services"></param>
    /// <param name="assemblies">List of assemblies to scan for role/permission declarations</param>
    public static IServiceCollection AddPermissions(this IServiceCollection services, params Assembly[] assemblies)
    {
        var rolePolicies = assemblies.GetRequiredRolePolicies();
        var claimPolicies = assemblies.GetRequiredClaimPolicies();

        services.AddAuthorizationCore(opt => {
            foreach (var claimPolicy in claimPolicies)
            {
                opt.AddPolicy(claimPolicy.Key, pol => pol.RequireClaim(claimPolicy.Value));
            }

            foreach (var rolePolicy in rolePolicies.Where(p => p.Value.Any()))
            {
                opt.AddPolicy(rolePolicy.Key, pol => pol.RequireRole(rolePolicy.Value));
            }
        });

        services.AddSingleton<IPermissionsProvider>(new AssemblyScanPermissionsProvider(assemblies));

        return services;
    }
}