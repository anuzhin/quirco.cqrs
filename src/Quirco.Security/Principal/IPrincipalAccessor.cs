﻿using System.Security.Principal;

namespace Quirco.Security.Principal;

/// <summary>
/// Factory to access current principal
/// </summary>
public interface IPrincipalAccessor
{
    /// <summary>
    /// Returns current principal
    /// </summary>
    IPrincipal GetPrincipal();
}