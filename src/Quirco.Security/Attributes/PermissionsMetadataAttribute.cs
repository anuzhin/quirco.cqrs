namespace Quirco.Security.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class PermissionsMetadataAttribute : Attribute
{
    public string ModuleName { get; set; }

    public PermissionsMetadataAttribute(string moduleName)
    {
        ModuleName = moduleName;
    }
}