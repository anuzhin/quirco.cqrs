namespace Quirco.Security.Permission;

public interface IPermissionsProvider
{
    IEnumerable<Permission> GetAllPermissions();
}