using Quirco.Security.Attributes;

namespace Quirco.Security.Permission;

/// <summary>
/// Хранилище пермишенов приложения. Заполняется путем сканирования всех сборок приложения
/// И поиска в них типов, декорированных атрибутом <see cref="PermissionsMetadataAttribute"/>.
/// </summary>
public class PermissionRegistry
{
    private readonly IEnumerable<IPermissionsProvider> _providers;

    public PermissionRegistry(IEnumerable<IPermissionsProvider> providers)
    {
        _providers = providers;
    }

    private Permission[]? _permissions;

    public Permission[] GetAll()
    {
        _permissions ??= _providers.SelectMany(p => p.GetAllPermissions()).ToArray();
        return _permissions;
    }
}

/// <summary>
/// Permission is a small piece of information about where user has access
/// </summary>
public record Permission
{
    /// <summary>
    /// User-readable module name
    /// </summary>
    public string ModuleName { get; set; }

    /// <summary>
    /// User-readable entity name
    /// </summary>
    public string EntityName { get; set; }
    
    /// <summary>
    /// User-readable full list of permissions 
    /// </summary>
    public string[] ReadablePermissions { get; set; }
    
    /// <summary>
    /// System identified permissions (formed as namespace + permission classes)
    /// </summary>
    public string[] Permissions { get; set; }

    public Permission(
        string moduleName,
        string entityName, 
        IEnumerable<string> readablePermissions,
        IEnumerable<string> permissions)
    {
        ModuleName = moduleName;
        EntityName = entityName;
        ReadablePermissions = readablePermissions.ToArray();
        Permissions = permissions.ToArray();
    }
}