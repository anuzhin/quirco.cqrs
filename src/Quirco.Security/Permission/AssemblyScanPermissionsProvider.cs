using System.ComponentModel;
using System.Reflection;
using Quirco.Security.Attributes;

namespace Quirco.Security.Permission;

/// <summary>
/// Ищет в сборках все типы декорированные атрибутом PermissionsData и формирует из них массив типа
/// "Имя подсистемы - Имя вложенного типа - Массив прав этого типа"
/// </summary>
public class AssemblyScanPermissionsProvider : IPermissionsProvider
{
    private readonly Assembly[] _assemblies;

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="assemblies">Сборки в которых будет происходит поиск</param>
    public AssemblyScanPermissionsProvider(params Assembly[] assemblies)
    {
        _assemblies = assemblies;
    }

    public IEnumerable<Permission> GetAllPermissions()
    {
        foreach (var asm in _assemblies)
        {
            foreach (var type in asm.GetTypes().Where(x => x.GetCustomAttribute(typeof(PermissionsMetadataAttribute)) != null))
            {
                foreach (var nestedType in type.GetNestedTypes())
                {
                    var readableModuleName = type.GetCustomAttribute<PermissionsMetadataAttribute>()!.ModuleName;
                    var readableEntityName = nestedType.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName ?? nestedType.Name;

                    var readablePermissions = new List<string>();
                    var permissions = new List<string>();

                    foreach (var property in nestedType.GetProperties())
                    {
                        var readablePropertyName = property.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName ?? property.Name;
                        var propertyName = property.Name;
                        readablePermissions.Add($"{readableModuleName}.{readableEntityName}.{readablePropertyName}");
                        permissions.Add($"{type.Namespace}.{nestedType.Name}.{propertyName}");
                    }
                    
                    var constants = nestedType.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
                    foreach (var property in constants.Where(fi => fi.IsLiteral && !fi.IsInitOnly))
                    {
                        var readablePropertyName = property.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName ?? property.Name;
                        readablePermissions.Add($"{readableModuleName}.{readableEntityName}.{readablePropertyName}");
                        permissions.Add((string)property.GetValue(null));
                    }

                    yield return new Permission(readableModuleName,
                        readableEntityName,
                        readablePermissions,
                        permissions);
                }
            }
        }
    }
}