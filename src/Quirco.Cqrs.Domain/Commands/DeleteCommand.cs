﻿using MediatR;
using Quirco.Cqrs.DataAccess.Models;

namespace Quirco.Cqrs.Domain.Commands;

/// <summary>
/// <inheritdoc />
/// </summary>
public class DeleteCommand<TEntity> : DeleteCommand<TEntity, int>
{
    protected DeleteCommand() : base() // Required for proper deserialization
    {
    }
        
    public DeleteCommand(int id) : base(id)
    {
    }
}

/// <summary>
/// Запрос на удаление сущности.
/// </summary>
/// <typeparam name="TId"></typeparam>
/// <typeparam name="TEntity"></typeparam>
public class DeleteCommand<TEntity, TId> : IRequest, IHasId<TId>
    where TId : struct
{
    public TId Id { get; set; }
        
    protected DeleteCommand() // Required for proper deserialization 
    {
    }
        
    /// <summary>
    /// Инициализирует новый экземпляр класса
    /// </summary>
    /// <param name="id"></param>
    public DeleteCommand(TId id)
    {
        Id = id;
    }
}