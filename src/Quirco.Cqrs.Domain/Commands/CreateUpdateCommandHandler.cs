using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.Exceptions;
using Quirco.Cqrs.Domain.Security;

namespace Quirco.Cqrs.Domain.Commands;

public abstract class CreateUpdateCommandHandler<TCommand, TContext, TModel, TEntity> : CreateUpdateCommandHandler<TCommand, TContext, TModel, TEntity, int>
    where TCommand : CreateUpdateCommand<int, TModel>
    where TContext : DbContext
    where TEntity : class, IHasId, new()
{
    protected CreateUpdateCommandHandler(TContext context, IMapper mapper, IEnumerable<IPermissionEvaluator<TEntity>> evaluators) : base(context, mapper,
        evaluators)
    {
    }
}

/// <summary>
/// Creates or updates existing entity
/// </summary>
public abstract class CreateUpdateCommandHandler<TCommand, TContext, TModel, TEntity, TId> : IRequestHandler<TCommand, TId>
    where TCommand : CreateUpdateCommand<TId, TModel>
    where TContext : DbContext
    where TEntity : class, IHasId<TId>, new()
    where TId : struct
{
    protected TContext Context { get; }
    protected IMapper Mapper { get; }
    protected IEnumerable<IPermissionEvaluator<TEntity>> Evaluators { get; }

    protected CreateUpdateCommandHandler(TContext context, IMapper mapper, IEnumerable<IPermissionEvaluator<TEntity>> evaluators)
    {
        Context = context;
        Mapper = mapper;
        Evaluators = evaluators;
    }

    public virtual async Task<TId> Handle(TCommand request, CancellationToken cancellationToken)
    {
        TEntity entity;
        if (request.Id == null)
        {
            entity = new TEntity();
            Context.Set<TEntity>().Add(entity);
            var id = entity.Id;
            await Map(request.Model, entity, cancellationToken);
            entity.Id = id;
            await ValidateInsertAccess(entity, cancellationToken);
        }
        else
        {
            entity = await Get(request, cancellationToken);

            await ValidateUpdateAccess(entity, cancellationToken);
            await Map(request.Model, entity, cancellationToken);
            entity.Id = request.Id.Value;
        }

        await Context.SaveChangesAsync(cancellationToken);
        await EntitySaved(request, entity, cancellationToken);
        return entity.Id;
    }

    /// <summary>
    /// Fires after entity changes saved to DB
    /// </summary>
    /// <param name="request"></param>
    /// <param name="entity"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    protected virtual Task EntitySaved(TCommand request, TEntity entity, CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

    /// <summary>
    /// Fires on updates, reads entity from DB
    /// </summary>
    /// <param name="request"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    protected virtual async Task<TEntity> Get(TCommand request, CancellationToken cancellationToken)
    {
        var entity = await Include(Context.Set<TEntity>())
            .FirstOrDefaultAsync(e => e.Id.Equals(request.Id), cancellationToken);
        if (entity == null)
            throw new ObjectNotFoundException<int>(typeof(TEntity), ""+request.Id);

        return entity;
    }
        
    /// <summary>
    /// Map model data onto existing entity 
    /// </summary>
    /// <param name="model"></param>
    /// <param name="entity"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    protected virtual Task Map(TModel model, TEntity entity, CancellationToken cancellationToken)
    {
        return Task.FromResult(Mapper.Map(model, entity));
    }
        
    /// <summary>
    /// Allows to include some properties to entity
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    protected virtual IQueryable<TEntity> Include(IQueryable<TEntity> query) => query;

    protected async Task ValidateInsertAccess(TEntity entity, CancellationToken cancellationToken)
    {
        foreach (var evaluator in Evaluators)
        {
            cancellationToken.ThrowIfCancellationRequested();
                
            if (!await evaluator.CanAdd(entity, cancellationToken))
            {
                throw new AccessDeniedException();
            }
        }
    }

    protected async Task ValidateUpdateAccess(TEntity entity, CancellationToken cancellationToken)
    {
        foreach (var evaluator in Evaluators)
        {
            cancellationToken.ThrowIfCancellationRequested();
                
            if (!await evaluator.CanUpdate(entity, cancellationToken))
            {
                throw new AccessDeniedException();
            }
        }
    }

}