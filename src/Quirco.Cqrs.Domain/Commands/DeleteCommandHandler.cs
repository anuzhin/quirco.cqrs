using MediatR;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.Exceptions;
using Quirco.Cqrs.Domain.Security;

namespace Quirco.Cqrs.Domain.Commands;

/// <inheritdoc />
public abstract class DeleteCommandHandler<TContext, TEntity> : DeleteCommandHandler<TContext, TEntity, int>
    where TContext : DbContext where TEntity : class, IHasId<int>, new()
{
    protected DeleteCommandHandler(TContext context, IEnumerable<IPermissionEvaluator<TEntity>>? evaluators) : base(context, evaluators)
    {
    }
}

/// <inheritdoc />
public abstract class DeleteCommandHandler<TContext, TEntity, TId> : DeleteCommandHandler<DeleteCommand<TEntity, TId>,
    TContext, TEntity, TId>
    where TEntity : class, IHasId<TId>, new()
    where TContext : DbContext
    where TId : struct
{
    protected DeleteCommandHandler(TContext context, IEnumerable<IPermissionEvaluator<TEntity>>? evaluators) : base(context, evaluators)
    {
    }
}

/// <summary>
/// Удаляет запись без её загрузки из базы данных, если не заданы валидаторы (evaluators)
/// </summary>
/// <typeparam name="TCommand"></typeparam>
/// <typeparam name="TContext"></typeparam>
/// <typeparam name="TEntity"></typeparam>
/// <typeparam name="TId"></typeparam>
public abstract class DeleteCommandHandler<TCommand, TContext, TEntity, TId> : IRequestHandler<TCommand>
    where TCommand : IRequest, IHasId<TId>
    where TEntity : class, IHasId<TId>, new()
    where TContext : DbContext
    where TId : struct
{
    protected TContext Context { get; }
    protected readonly IEnumerable<IPermissionEvaluator<TEntity>>? Evaluators;

    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="DeleteCommandHandler{TCommand, TContext, TEntity, TId}"/>.
    /// </summary>
    protected DeleteCommandHandler(TContext context, IEnumerable<IPermissionEvaluator<TEntity>>? evaluators)
    {
        Context = context;
        Evaluators = evaluators;
    }

    public virtual async Task<Unit> Handle(TCommand request, CancellationToken cancellationToken)
    {
        var entity = await Context
                         .Set<TEntity>()
                         .FirstOrDefaultAsync(e => e.Id.Equals(request.Id), cancellationToken) ??
                     throw new ObjectNotFoundException<TId>(typeof(TEntity), request.Id);
        await ValidateAccessAsync(entity, cancellationToken);
        if (entity is IHasDateDeleted hasDateDeleted)
        {
            hasDateDeleted.DateDeleted = DateTime.UtcNow;
        }
        else
        {
            Context.Entry(entity).State = EntityState.Deleted;
        }

        await Context.SaveChangesAsync(cancellationToken);
        return Unit.Value;
    }

    private async Task ValidateAccessAsync(TEntity entity, CancellationToken cancellationToken)
    {
        if (Evaluators != null)
        {
            foreach (var evaluator in Evaluators)
            {
                cancellationToken.ThrowIfCancellationRequested();

                if (!await evaluator.CanDelete(entity, cancellationToken))
                {
                    throw new AccessDeniedException();
                }
            }
        }
    }
}