using MediatR;
using Quirco.Cqrs.Domain.Models;

namespace Quirco.Cqrs.Domain.Queries;

/// <summary>
/// Поисковый запрос для получения списка сущностей.
/// </summary>
/// <typeparam name="TModel"></typeparam>
public abstract class ListQuery<TModel> : LimitQuery, IRequest<PagedResponse<TModel>>
{
}