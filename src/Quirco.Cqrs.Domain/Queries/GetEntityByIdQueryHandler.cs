using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.Exceptions;
using Quirco.Cqrs.Domain.Extensions;
using Quirco.Cqrs.Domain.Filters;

namespace Quirco.Cqrs.Domain.Queries;

public abstract class GetEntityByIdQueryHandler<TQuery, TModel, TEntity, TContext> : GetEntityByIdQueryHandler<TQuery, TModel, TEntity, TContext, int>
    where TContext : DbContext
    where TEntity : class, IHasId
    where TQuery : GetEntityByIdQuery<TModel, int>
{
    protected GetEntityByIdQueryHandler(IMapper mapper, TContext context, IEnumerable<IQueryableModifier<TEntity>> modifiers) : base(mapper, context,
        modifiers)
    {
    }
}

/// <summary>
/// Get model by ID
/// </summary>
/// <typeparam name="TModel">Type of model object</typeparam>
/// <typeparam name="TEntity">Type of entity</typeparam>
/// <typeparam name="TContext">Data context</typeparam>
/// <typeparam name="TQuery">Query type</typeparam>
/// <typeparam name="TId">Type of identifier</typeparam>
public abstract class GetEntityByIdQueryHandler<TQuery, TModel, TEntity, TContext, TId> : IRequestHandler<TQuery, TModel>
    where TContext : DbContext
    where TEntity : class, IHasId<TId>
    where TQuery : GetEntityByIdQuery<TModel, TId>
{
    /// <summary>
    /// Инициализирует новый экземпляр класса
    /// </summary>
    /// <param name="mapper"></param>
    /// <param name="context"></param>
    /// <param name="modifiers"></param>
    protected GetEntityByIdQueryHandler(
        IMapper mapper,
        TContext context,
        IEnumerable<IQueryableModifier<TEntity>> modifiers)
    {
        Mapper = mapper;
        Context = context;
        Modifiers = modifiers;
    }

    /// <summary>
    /// Контекст работы с БД.
    /// </summary>
    protected TContext Context { get; }

    /// <summary>
    /// Маппер сущностей.
    /// </summary>
    protected IMapper Mapper { get; }

    /// <summary>
    /// Модификаторы запроса.
    /// </summary>
    protected IEnumerable<IQueryableModifier<TEntity>> Modifiers { get; }

    /// <inheritdoc />
    public virtual async Task<TModel> Handle(TQuery request, CancellationToken cancellationToken)
    {
        return await Get(request, cancellationToken) ?? throw new ObjectNotFoundException<TId>(typeof(TEntity), request.Id);
    }

    /// <summary>
    /// Получить сущность по идентификатору.
    /// </summary>
    /// <param name="query"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    protected virtual async Task<TModel> Get(TQuery query, CancellationToken token)
    {
        var queryable = await Include(Context.Set<TEntity>()).ApplyModifiers(Modifiers, token);
        return await MapQueryableToModel(queryable.Where(e => e.Id.Equals(query.Id)), token);
    }

    /// <summary>
    /// Maps queryable with single entity (already filtered by it's ID) to model using projection.
    /// Can be overriden for complex cases when include is needed 
    /// </summary>
    /// <returns></returns>
    protected virtual async Task<TModel> MapQueryableToModel(IQueryable<TEntity> queryable, CancellationToken token)
    {
        return await queryable
            .ProjectTo<TModel>(Mapper.ConfigurationProvider)
            .SingleOrDefaultAsync(token);
    }

    /// <summary>
    /// Включить необходимый навигационные свойства.
    /// </summary>
    /// <param name="query"></param>
    /// <returns></returns>
    protected virtual IQueryable<TEntity> Include(IQueryable<TEntity> query) => query;
}