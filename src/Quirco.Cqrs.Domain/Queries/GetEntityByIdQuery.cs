﻿using System.ComponentModel;
using MediatR;

namespace Quirco.Cqrs.Domain.Queries;

public abstract class GetEntityByIdQuery<T> : GetEntityByIdQuery<T, int>
{
    protected GetEntityByIdQuery(int id) : base(id)
    {
    }
}
    
/// <summary>
/// Get entity model by id
/// </summary>
/// <typeparam name="T">Type of entity</typeparam>
/// <typeparam name="TId">Type of identifier</typeparam>
public abstract class GetEntityByIdQuery<T, TId> : IRequest<T>
{
    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="GetEntityByIdQuery{T}"/>.
    /// </summary>
    /// <param name="id"></param>
    public GetEntityByIdQuery(TId id)
    {
        Id = id;
    }
        
    /// <summary>
    /// Идентификатор сущности.
    /// </summary>
    [DisplayName("Идентификатор сущности")]
    public TId Id { get; set; }
}