namespace Quirco.Cqrs.Domain.Queries;

/// <summary>
/// Search by query text
/// </summary>
/// <typeparam name="TModel"></typeparam>
public abstract class SearchQuery<TModel> : ListQuery<TModel>
{
    /// <summary>
    /// Search query (term)
    /// </summary>
    public string? Query { get; set; }
}