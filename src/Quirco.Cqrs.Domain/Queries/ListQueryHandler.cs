using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Quirco.Cqrs.DataAccess.Models;
using Quirco.Cqrs.Domain.DataContextExtensions;
using Quirco.Cqrs.Domain.Extensions;
using Quirco.Cqrs.Domain.Filters;
using Quirco.Cqrs.Domain.Models;

namespace Quirco.Cqrs.Domain.Queries;

public abstract class ListQueryHandler<TModel, TEntity, TContext, TListQuery> 
    : ListQueryHandler<TModel, TEntity, TContext, TListQuery, int>
    where TContext : DbContext
    where TEntity : class, IHasId
    where TListQuery : ListQuery<TModel>
{
    protected ListQueryHandler(IMapper mapper, IDbContextFactory<TContext> context,
        IEnumerable<IQueryableModifier<TEntity>> modifiers) : base(mapper, context, modifiers)
    {
    }
}

/// <summary>
/// Обработчик запроса на поиск элементов.
/// </summary>
/// <typeparam name="TListQuery">Тип поискового запроса.</typeparam>
/// <typeparam name="TModel">Тип модели.</typeparam>
/// <typeparam name="TEntity">Тип сущности в БД.</typeparam>
/// <typeparam name="TContext">Тип контекста БД.</typeparam>
/// <typeparam name="TId"></typeparam>
public abstract class ListQueryHandler<TModel, TEntity, TContext, TListQuery, TId> 
    : IRequestHandler<TListQuery, PagedResponse<TModel>>
    where TEntity : class, IHasId<TId>
    where TContext : DbContext
    where TListQuery : ListQuery<TModel>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ListQueryHandler{TModel, TEntity, TContext, TListQuery}"/>.
    /// </summary>
    /// <param name="mapper"></param>
    /// <param name="contextFactory"></param>
    /// <param name="modifiers"></param>
    protected ListQueryHandler(IMapper mapper, IDbContextFactory<TContext> contextFactory, IEnumerable<IQueryableModifier<TEntity>> modifiers)
    {
        Mapper = mapper;
        ContextFactory = contextFactory;
        Modifiers = modifiers;
    }

    /// <summary>
    /// Маппер сущностей.
    /// </summary>
    protected IMapper Mapper { get; }

    /// <summary>
    /// Контекст работы с БД.
    /// </summary>
    protected IDbContextFactory<TContext> ContextFactory { get; }

    /// <summary>
    /// Модификаторы запроса.
    /// </summary>
    protected IEnumerable<IQueryableModifier<TEntity>> Modifiers { get; }

    /// <inheritdoc />
    public virtual async Task<PagedResponse<TModel>> Handle(TListQuery request, CancellationToken cancellationToken)
    {
        await using var context = ContextFactory.CreateDbContext();
        var query = await BuildQuery(context, request, cancellationToken);
        var list = await MapQueryableToModel(request, query.Paging<TEntity, TId>(request), cancellationToken);
        var response = new PagedResponse<TModel>(list, await query.CountAsync(cancellationToken));
        return response;
    }

    protected virtual async Task<TModel[]> MapQueryableToModel(TListQuery request, IQueryable<TEntity> query, CancellationToken cancellationToken)
    {
        return await query
            .ProjectTo<TModel>(Mapper.ConfigurationProvider)
            .ToArrayAsync(cancellationToken);
    }

    /// <summary>
    /// Добавить специфичные запросу фильтры.
    /// </summary>
    /// <returns></returns>
    protected virtual async Task<IQueryable<TEntity>> BuildQuery(TContext context, TListQuery request, CancellationToken cancellationToken)
    {
        var query = context
            .Set<TEntity>()
            .AsQueryable()
            .AsNoTracking();

        query = await query.ApplyModifiers(Modifiers, cancellationToken);
        return query.OrderBy(e => e.Id);
    }
}