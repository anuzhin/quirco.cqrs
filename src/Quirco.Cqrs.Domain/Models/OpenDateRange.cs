namespace Quirco.Cqrs.Domain.Models;

/// <summary>
/// Открытый интервал дат
/// </summary>
public class OpenDateRange
{
    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="OpenDateRange"/>.
    /// </summary>
    public OpenDateRange()
    {
    }
        
    /// <summary>
    /// Инициализирует новый экземпляр класса <see cref="OpenDateRange"/>.
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    public OpenDateRange(DateTime? @from, DateTime? to)
    {
        From = @from;
        To = to;
    }
        
    /// <summary>
    /// Начало отрезка включительно.
    /// </summary>
    public DateTime? From { get; set; }
        
    /// <summary>
    /// Конец отрезка включительно.
    /// </summary>
    public DateTime? To { get; set; }
}