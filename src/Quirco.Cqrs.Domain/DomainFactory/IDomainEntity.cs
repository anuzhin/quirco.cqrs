namespace Quirco.Cqrs.Domain.DomainFactory;

[Obsolete]    
public interface IDomainEntity<T>
{
    T Entity { get; }
    void SetEntity(T entity);
}