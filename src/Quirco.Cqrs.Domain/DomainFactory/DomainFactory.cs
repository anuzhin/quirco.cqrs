using Microsoft.Extensions.DependencyInjection;

namespace Quirco.Cqrs.Domain.DomainFactory;

[Obsolete]    
public class DomainFactory : IDomainFactory
{
    private readonly IServiceProvider _serviceProvider;

    public DomainFactory(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public TDomainEntity GetEntity<TDomainEntity, TEntity>(TEntity entity) where TDomainEntity : DomainEntity<TEntity>
    {
        var domainEntity = _serviceProvider.GetRequiredService<TDomainEntity>();
        ((IDomainEntity<TEntity>) domainEntity).SetEntity(entity);
        return domainEntity;
    }
}