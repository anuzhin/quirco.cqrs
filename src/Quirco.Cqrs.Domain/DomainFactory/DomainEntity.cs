namespace Quirco.Cqrs.Domain.DomainFactory;

/// <summary>
/// Root entity from bounded context.
/// </summary>
[Obsolete]
public abstract class DomainEntity<T> : IDomainEntity<T>
{
    private T _entity;
    public T Entity => _entity;

    void IDomainEntity<T>.SetEntity(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException(nameof(entity));
        _entity = entity;
    }
}