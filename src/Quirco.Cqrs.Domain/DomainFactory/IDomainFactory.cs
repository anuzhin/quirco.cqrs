namespace Quirco.Cqrs.Domain.DomainFactory;

[Obsolete]
public interface IDomainFactory
{
    TDomainEntity GetEntity<TDomainEntity, TEntity>(TEntity entity) where TDomainEntity : DomainEntity<TEntity>;
}