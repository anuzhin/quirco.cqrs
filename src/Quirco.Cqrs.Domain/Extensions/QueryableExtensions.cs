﻿using Quirco.Cqrs.Domain.Filters;

namespace Quirco.Cqrs.Domain.Extensions;

public static class QueryableExtensions
{
    public static async Task<IQueryable<TEntity>> ApplyModifiers<TEntity>(this IQueryable<TEntity> query, 
        IEnumerable<IQueryableModifier<TEntity>>? modifiers, CancellationToken cancellationToken) 
        where TEntity: class
    {
        if (modifiers == null) return query;
            
        foreach (var modifier in modifiers)
            query = await modifier.Modify(query);

        return query;
    }
}