﻿namespace Quirco.Cqrs.Domain.Security;

/// <summary>
/// CRUD-restriction policy
/// </summary>
/// <typeparam name="TEntity"></typeparam>
public interface IPermissionEvaluator<in TEntity> where TEntity: class
{
    /// <summary>
    /// Checks if entity can be added
    /// </summary>
    /// <param name="entity"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<bool> CanAdd(TEntity entity, CancellationToken cancellationToken);

    /// <summary>
    /// Checks if user can update entity
    /// </summary>
    /// <param name="entity"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<bool> CanUpdate(TEntity entity, CancellationToken cancellationToken);

    /// <summary>
    /// Checks if user can delete entity
    /// </summary>
    /// <param name="entity"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<bool> CanDelete(TEntity entity, CancellationToken cancellationToken);
}