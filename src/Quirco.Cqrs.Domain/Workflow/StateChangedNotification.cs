using MediatR;

namespace Quirco.Cqrs.Domain.Workflow;

/// <summary>
/// Уведомление об изменении состояния.
/// </summary>
/// <typeparam name="TState">Тип состояния.</typeparam>
/// <typeparam name="TEntity">Тип сущности.</typeparam>
public class StateChangedNotification<TState, TEntity> : INotification
{
    /// <summary>
    /// Прежнее состояние.
    /// </summary>
    public TState PreviousState { get; set; }

    /// <summary>
    /// Новое состояние.
    /// </summary>
    public TState NewState { get; set; }

    /// <summary>
    /// Сущность.
    /// </summary>
    public TEntity Entity { get; set; }
}