### Validation

To add fluent validation to MediatR pipeline simply register validation pipeline in container:
```csharp
services.AddScoped(typeof(IPipelineBehavior<,>), typeof(FluentValidationPipelineBehavior<,>));
```
