# Quirco.Cqrs

CQRS patterns and reusable parts of code to build scalable applications.

These libraries covers different layers and parts of application and described below.
Most of features are based on following 3rd party packages:
- AutoMapper
- FluentValidation
- MediatR
- EF Core
- Hangfire
- xUnit
- NEST

## Domain

Commands, queries, some base models for CQRS-based business domain layer. Includes base abastract commands for `CreateUpdate`, `Delete` and queries for `GetEntityById`, `ListQuery`, `SearchQuery`. All of these base implementations are abstract so you'll need to declare implicit handlers for your entities/models.

Includes `IAffectEntityCommand` interface so that some pipeline logging logic can enrich log records with entity type and id. 

### Exceptions
This package contains some common useful base exceptions:
- `AccessDeniedException` - thrown on authorization failures
- `BusinessRuleException` - any custom business rule violation
- `ConcurrencyException` - thrown on concurrent update of EF entity
- `ObjectNotFoundException` - thrown when entity is not found in data context


## DataAccess

Package contains reusable parts for data access layer. Main features are described below.

### Base interfaces for entities
Several common interfaces were introduced to unify processing of entites. These are:
- `Entity<T>` - base entity class with `Id` of type `T`
- `IHasCode` - entity that has string `Code` field
- `IHasDateCreated` - entity with `DateCreated` field that contains creation date
- `IHasDateDeleted` - entity with `DateDeleted` field that contains deletion date and supports soft deletion mechanism
- `IHasDateModified` - entity with `DateModified` field that contains last modificatio date (when any of the field was changed)
- `IHasDraft` - entity with `IsDraft` flag indicating that entity is not ready to be presented to end users and is somewhere on middle of process
- `IHasId<T>` - interface for classes that has `Id` field of type `T`. Can be used both for entities and models

### Auditable DbContext
`DbContextAuditor` class allows to easily monitor changes in context via set of events. When entity of specified type will be updated or deleted, corresponding events will be sent to `MediatR` pipeline (as notifications). 

Events are:
`EntityUpdatingNotification` - fires before saving changes to database.
`EntityUpdatedNotification` - fires right after save.

Events are fired only for really changed entries (auditor uses `ChangeTracker` for this). Each event has extra flag, indicating if entity was inserted, updated or deleted.

Minimal configuration example:
```csharp
public class PaymentDbContext {
    private DbContextAuditor _auditor;
    public PaymentDbContext(IMediator mediator) {
        // Setup auditor to track certain entities
        _auditor = new DbContextAuditor()
            .AddAuditableEntity<Customer>()
            .AddAuditableEntity<Payment>();        
    }

    public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        // Override save procedure so that auditor will be able to track changes before and after save
        return await _auditor.SaveChanges((ct) => base.SaveChangesAsync(ct), cancellationToken);
    }
}

```


### Behaviors
`TransactionAttribute` - allows to mark command with `[Transaction]` attribute, to enable `TransactionPipelineBehavior` to automatically wrap execution of command handler into single transaction.

```csharp
[Transaction(IsolationLevel=TransactionIsolationLevel.Serializable)]
public class ProcessPaymentCommand {
    public int PaymentId { get; }
    public ProcessPaymentCommand(int paymentId) {
        PaymentId = paymentId;
    }
}
```
So you shouldn't care about transactions in your handlers and keep code clearer.

## Infrastructure

Contains lot of things, better to look at sources. Most useful are: `IJobsService` - abstraction around any background job library. This package contains [Hangfire](##Hangfire) implementation of this interface.

### Current date abstraction
`ICurrentDateProvider` is used to make abstraction from `DateTime.Now` for easier testing. Package contains two implementations:
- `CurrentSystemDateProvider` that returns `DateTime.Now`, and
- `CurrentSystemUtcDateProvider` returning `DateTime.UtcNow`

For testing purposes see [Tests](##Tests) section


## Hangfire

Contains `Hangfire` implementation for `IJobsService` abstraction. This interface allows you to schedule and enqueue background jobs.
You'll need to register service:

```csharp
services.AddScoped<IJobsService, HangfireJobsService>();
```

## Mvc

### Controllers
Includes base implementation for CRUD controller and code generator template, that can be copied to your project and will allow quickly generate CRUD controllers for simple entities.


### Filters
Filters includes all common processing of exceptions. For example, they contains `AccessDeniedExceptionFilter` that will turn `AccessDeniedException` from domain layer into 401 response. Filters are:
- `AccessDeniedExceptionFilter`
- `ConcurrencyExceptionFilter` - will convert concurrency errors into 409 response codes
- `ObjectNotFoundExceptionFilter` will return 404 when no entity found
- `OperationCancelledExceptionFilter` is used to mute operation cancelled errors from main application log, will return 400 code to client
- `ValidationExceptionFilter` handles `ValidationException` and turns it info detailed 400 response

## Elasticsearch
Useful commands and behaviors to wrap some queries in CQRS pattern and return result not from database, but from Elasticsearch index.
To intercept queries you should inherit and implement `SearchPipelineBehavior`.


## Security
`Quirco.Security` package is designed to provide general permission-based security system. See `Tests/Security` for more information and examples.
